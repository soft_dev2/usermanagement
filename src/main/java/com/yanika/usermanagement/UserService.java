/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.yanika.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author ASUS
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    static {
        userList.add(new User("admin", "password"));
        userList.add(new User("user1", "password"));
    }
    //Create

    public static boolean addUser(User user) {
        userList.add(user);
        return true;
    }

    public static boolean addUser(String userName, String Password) {
        userList.add(new User(userName, Password));
        return true;
    }

    public static boolean updateUser(int index, User user) { //update user in List , + index?
        userList.set(index, user);
        return true;
    }
    //Read 1 user 
    public static User getUser (int index){// get
        if(index>userList.size()-1){
            return null ;
        }
        return userList.get(index);
    }
     //Read all user 
    public static ArrayList<User> getUsers(){// get
        return userList;
    }
         //search username 
    public static ArrayList<User> seatchUsersName(String searchText){// get
        ArrayList<User> list = new ArrayList<>();
        for(User user: userList){
            if(user.getUserName().startsWith(searchText)){
                list.add(user);
            }
        }
        return list;
    }
    
    //Delete user
    public static boolean delUser (int index){
        userList.remove(index);
        
        return true;
    }
    
        //Delete user
    public static boolean delUser (User user){
        userList.remove(user);
        return true;
    }
    
    // Login
    public static User login(String UserName, String password){
       for(User user: userList){
           if(user.getUserName().equals(UserName)&& user.getPassword().equals(password)){
               return user;
           }
       }
        return null ;
    }
    
    public static void save(){
        
    }
   
     public static void load(){
        
    }
    
}
